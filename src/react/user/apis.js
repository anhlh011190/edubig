import { post } from "./../base/portal";

export function login(username, password){
    let params = {
        username,
        password
    }
    return post("https://api.edubig.vn/api/v2/user/login", params);
}

export function signUp(username, password){
    let params = {
        username,
        password
    }
    return post("https://api.edubig.vn/api/v2/user", params);
}