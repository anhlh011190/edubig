
import { MiddlewareRegistry }   from '../base/redux';

MiddlewareRegistry.register(store => next => action => {
    switch (action.type) {
        default:
            return next(action);
    }
});