import { 
    login as loginApi,
    signUp as signUpApi
}                               from './apis';
import { 
    changeUserInfo
}                               from './../base/app';

import {
    setAsyncStorage
}                               from './../base/portal';

export function login(username, password) {
    return function (dispatch, getState) {
        loginApi(username, password).then((rsLogin) => {
            if(rsLogin.success){
                setAsyncStorage("USER",JSON.stringify({username, password}));
                dispatch(changeUserInfo({...rsLogin.result, isLogin: true}));
            }else{
                alert(rsLogin.message);
            }
        })
    }
}

export function signUp(username, password) {
    return function (dispatch, getState) {
        signUpApi(username, password).then((rsSignUp) => {
            if(rsSignUp.success){
                setAsyncStorage("USER",JSON.stringify({username, password}));
                dispatch(changeUserInfo({...rsSignUp.result, isLogin: true}));
            }else{
                alert(rsSignUp.message);
            }
        })
    }
}