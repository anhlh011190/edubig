import React                from 'react';
import { connect }          from 'react-redux';
import {
    TouchableHighlight,
    View,
    Text
}                           from 'react-native';
import { 
    signOut,
}                           from './../../base/app';

class UserHeaderAuth extends React.Component{
    constructor(props){
        super(props);
    }

    signOut = () => {
        this.props.dispatch(signOut());
    }

    render(){
        return(
            <View style={{flex: 8}}>
                <Text style={{lineHeight: 25}}>Xin chào: {this.props.userInfo.fullname ? this.props.userInfo.fullname: this.props.userInfo.username}</Text>
                <TouchableHighlight
                    accessibilityLabel={'Tap to Join.'}
                    onPress={this.signOut}
                    >
                    <Text style={{lineHeight: 25}}>Đăng xuất</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

function _mapStateToProps(state) {
    return {
        userInfo: state['edubig/app'].userInfo,
    };
}

export default connect(_mapStateToProps)(UserHeaderAuth);