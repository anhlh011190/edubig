import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView,
    View, TouchableHighlight,
    Text,
    StatusBar
}                           from 'react-native';
import { 
    BottomBar, Header
}                           from './../../homepage';


class SignIn extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.authListener();
    }

    authListener() {
        firebase.auth().onAuthStateChanged((u) => {
            if (u) {
                console.log(u);
            } else {
                console.log("Error");
            }
        });
    }

    signUp = () => {
        let email = "fb.edubig.vn@gmail.com";
        let password = "123456";
    }

    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5}}>
                    <View style={{height: 30, flexDirection: "row"}}>
                        <View style={{flex: 6}}>
                            <Text style={{textTransform:"uppercase"}}>Login</Text>
                        </View>
                        <TouchableHighlight onPress={this.signUp}>
                            <Text style={{color: "#000"}}>SignIn</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
    };
}

export default connect(_mapStateToProps)(SignIn);