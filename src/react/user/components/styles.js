import { StyleSheet }         from 'react-native';

const styles = StyleSheet.create({
    menu: {paddingHorizontal: 5, flexDirection: "row", marginTop: 5},
    menuBtn:{flex: 1},
    menuText:{textAlign: 'center', backgroundColor: "#ccc", marginRight: 1, paddingVertical: 4},
    menuTextActive: {backgroundColor: "#686"},
    loginForm: {
        flexDirection: 'column',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        marginTop: 100
    },
    loginFormInput: {
        flexDirection: 'row',
        width: 250,
        height: 40,
        backgroundColor: 'rgba(1,1,1,0.3)',
        borderRadius: 20,
        marginBottom: 25,
        alignItems:'center'
    },
    loginFormInputIcon: {
        flex: 1.5,
        marginLeft: 12,
        color: '#fff',
        fontSize: 20
    },
    loginFormInputIconText: {
        color: 'white',
        fontSize: 16,
        flex: 8.5
    },
    loginBtn: {
        backgroundColor: '#f6c108',
        borderRadius: 20,
        height: 40,
        width: 250,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        elevation: 5
    },
    loginBtnText: {
        alignSelf: 'center',
        color: '#001a29',
        fontSize: 15,
        textAlign: 'center',
        fontWeight: '600'
    }
});

  export default styles;
