import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView, ScrollView,
    View, StyleSheet, Dimensions,
    Text, TextInput,
    StatusBar, TouchableHighlight
}                           from 'react-native';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';
import { 
    BottomBar, Header
}                           from './../../homepage';
import {
    changeScreen
}                           from './../../base/app';
import { 
    signOut,
}                           from './../../base/app';
import {
    login,
    signUp
}                           from './../actions';

class Dashboard extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: "",
            rePassword: "",
            control: 0
        }
    }

    signIn = () => {
        this.props.dispatch(login(this.state.username, this.state.password));
    }

    signUp = () => {
        if(this.state.password.length > 5 &&this.state.rePassword == this.state.password){
            this.props.dispatch(signUp(this.state.username, this.state.password));
        }else{
            alert("Thông tin đăng ký chưa hợp lệ!")
        }
    }

    onViewEbook = (screen,data) => () => {
        this.props.dispatch(changeScreen("Ebooks"));
        this.props.navigation.navigate(screen, data);
    }

    signOut = () => {
        this.props.dispatch(signOut());
    }

    _onUsernameChange = (username) => {
        this.setState({ username });
    }

    _onPasswordChange = (password) => {
        this.setState({ password });
    }

    _onRePasswordChange = (rePassword) => {
        this.setState({ rePassword });
    }

    onPress = (screen) => () => {
        this.props.dispatch(changeScreen("Dashboard"));
        this.props.navigation.navigate(screen);
    }

    changeControl = control => () => {
        this.setState({control});
    }

    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5}}>
                    <View style={{flexDirection: "row", paddingTop: 10}}>
                        <View style={{paddingLeft:20, flex: 3}}>
                            <FontAwesome style={{fontSize: 60, color: "#00b386"}} name='user-circle' />
                        </View>
                        {
                            !this.props.userInfo.isLogin ?
                            <View style={{flex: 8}}>
                                <TouchableHighlight
                                    accessibilityLabel={'Tap to Join.'}
                                    onPress={this.changeControl(1)}
                                    >
                                    <Text style={{lineHeight: 25}}>Đăng nhập</Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    accessibilityLabel={'Tap to Join.'}
                                    onPress={this.changeControl(2)}
                                    >
                                    <Text style={{lineHeight: 25}}>Đăng ký</Text>
                                </TouchableHighlight>
                            </View>
                            :
                            <View style={{flex: 8}}>
                                <Text style={{lineHeight: 25}}>Xin chào: {this.props.userInfo.fullname ? this.props.userInfo.fullname: this.props.userInfo.username}</Text>
                                <TouchableHighlight
                                    accessibilityLabel={'Tap to Join.'}
                                    onPress={this.signOut}
                                    >
                                    <Text style={{lineHeight: 25}}>Đăng xuất</Text>
                                </TouchableHighlight>
                            </View>
                        }
                    </View>
                    <View style={styles.menu}>
                        <TouchableHighlight
                            accessibilityLabel={'Tap to Join.'}
                            onPress={this.changeControl(0)}
                            style={styles.menuBtn}>
                            <Text style={this.state.control == 0 ? [styles.menuText,styles.menuTextActive] : styles.menuText}>Lịch sử</Text>
                        </TouchableHighlight>
                        {
                            this.props.userInfo.isLogin ?
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.changeControl(3)}
                                style={styles.menuBtn}>
                                <Text style={this.state.control == 3 ? [styles.menuText,styles.menuTextActive] : styles.menuText}>Bạn bè</Text>
                            </TouchableHighlight>
                            :
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.changeControl(1)}
                                style={styles.menuBtn}>
                                <Text style={this.state.control == 1 ? [styles.menuText,styles.menuTextActive] : styles.menuText}>Đăng nhập</Text>
                            </TouchableHighlight>
                        }
                        {
                            this.props.userInfo.isLogin ?
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.changeControl(4)}
                                style={styles.menuBtn}>
                                <Text style={this.state.control == 4 ? [styles.menuText,styles.menuTextActive] : styles.menuText}>Thông tin</Text>
                            </TouchableHighlight>
                            :
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.changeControl(2)}
                                style={styles.menuBtn}>
                                <Text style={this.state.control == 2 ? [styles.menuText,styles.menuTextActive] : styles.menuText}>Đăng ký</Text>
                            </TouchableHighlight>
                        }
                    </View>
                    {
                        (this.state.control == 1 && !this.props.userInfo.isLogin)&& 
                        <View style={styles.loginForm}>
                            <Text style={{lineHeight: 25, fontSize: 25, marginBottom: 10}}>Thông tin đăng nhập</Text>
                            <View style={styles.loginFormInput} >
                                <FontAwesome
                                    name="envelope"
                                    style={styles.loginFormInputIcon}
                                />
                                <TextInput
                                    accessibilityLabel={'Tên đăng nhập'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    autoFocus={false}
                                    onChangeText={this._onUsernameChange}
                                    placeholder={"Tên đăng nhập"}
                                    placeholderTextColor='gray'
                                    style={styles.loginFormInputIconText}
                                    underlineColorAndroid='transparent'
                                    value={this.state.username} />
                            </View>
                            <View style={styles.loginFormInput} >
                                <FontAwesome
                                    name="lock"
                                    style={styles.loginFormInputIcon}
                                />
                                <TextInput
                                    accessibilityLabel={'Mật khẩu'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    autoFocus={false}
                                    onChangeText={this._onPasswordChange}
                                    placeholder={"Mật khẩu"}
                                    placeholderTextColor='gray'
                                    secureTextEntry
                                    style={styles.loginFormInputIconText}
                                    underlineColorAndroid='transparent'
                                    value={this.state.password} />
                            </View>
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.signIn}
                                style={styles.loginBtn}>
                                <Text style={styles.loginBtnText}>Đăng nhập</Text>
                            </TouchableHighlight>
                        </View>
                    }
                    {
                        this.state.control == 0 &&
                        <ScrollView style={{width: (Dimensions.get('window').width-20)}}>
                        {
                            this.props.ebooks != undefined &&
                            this.props.ebooks.map((e,index)=>{
                                return(
                                    <TouchableHighlight onPress={this.onViewEbook("Ebook",e)} key={index}>
                                        <View style={{flexDirection:"row", paddingHorizontal: 10, width: "100%", marginTop: 5}}>
                                            <FontAwesome style={{fontSize: 10, marginRight: 10, marginTop: 5}} name='circle' />
                                            <Text style={{color: "#000"}}>{e.title}</Text>
                                        </View>
                                    </TouchableHighlight>
                                )
                            })
                        }
                        </ScrollView>
                    }
                    {
                        (this.state.control == 2 && !this.props.userInfo.isLogin)&& 
                        <View style={styles.loginForm}>
                            <Text style={{lineHeight: 25, fontSize: 25, marginBottom: 10}}>Thông tin đăng ký</Text>
                            <View style={styles.loginFormInput} >
                                <FontAwesome
                                    name="envelope"
                                    style={styles.loginFormInputIcon}
                                />
                                <TextInput
                                    accessibilityLabel={'Tên đăng nhập'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    autoFocus={false}
                                    onChangeText={this._onUsernameChange}
                                    placeholder={"Tên đăng nhập"}
                                    placeholderTextColor='gray'
                                    style={styles.loginFormInputIconText}
                                    underlineColorAndroid='transparent'
                                    value={this.state.username} />
                            </View>
                            <View style={styles.loginFormInput} >
                                <FontAwesome
                                    name="lock"
                                    style={styles.loginFormInputIcon}
                                />
                                <TextInput
                                    accessibilityLabel={'Mật khẩu'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    autoFocus={false}
                                    onChangeText={this._onPasswordChange}
                                    placeholder={"Mật khẩu"}
                                    placeholderTextColor='gray'
                                    style={styles.loginFormInputIconText}
                                    secureTextEntry
                                    underlineColorAndroid='transparent'
                                    value={this.state.password} />
                            </View>
                            <View style={styles.loginFormInput} >
                                <FontAwesome
                                    name="lock"
                                    style={styles.loginFormInputIcon}
                                />
                                <TextInput
                                    accessibilityLabel={'Nhập lại mật khẩu'}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    autoFocus={false}
                                    onChangeText={this._onRePasswordChange}
                                    placeholder={"Nhập lại mật khẩu"}
                                    placeholderTextColor='gray'
                                    style={styles.loginFormInputIconText}
                                    secureTextEntry
                                    underlineColorAndroid='transparent'
                                    value={this.state.rePassword} />
                            </View>
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.signUp}
                                style={styles.loginBtn}>
                                <Text style={styles.loginBtnText}>Đăng ký</Text>
                            </TouchableHighlight>
                        </View>
                    }
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

let styles = StyleSheet.create({
    menu: {paddingHorizontal: 5, flexDirection: "row", marginTop: 5},
    menuBtn:{flex: 1},
    menuText:{textAlign: 'center', backgroundColor: "#ccc", marginRight: 1, paddingVertical: 4},
    menuTextActive: {backgroundColor: "#686"},
    loginForm: {
        flexDirection: 'column',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        marginTop: 100
    },
    loginFormInput: {
        flexDirection: 'row',
        width: 250,
        height: 40,
        backgroundColor: 'rgba(1,1,1,0.3)',
        borderRadius: 20,
        marginBottom: 25,
        alignItems:'center'
    },
    loginFormInputIcon: {
        flex: 1.5,
        marginLeft: 12,
        color: '#fff',
        fontSize: 20
    },
    loginFormInputIconText: {
        color: 'white',
        fontSize: 16,
        flex: 8.5
    },
    loginBtn: {
        backgroundColor: '#f6c108',
        borderRadius: 20,
        height: 40,
        width: 250,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        elevation: 5
    },
    loginBtnText: {
        alignSelf: 'center',
        color: '#001a29',
        fontSize: 15,
        textAlign: 'center',
        fontWeight: '600'
    }
});

function _mapStateToProps(state) {
    let ebooks = state['edubig/ebook'].logs;
    ebooks.sort(function(b,a){return a.time - b.time})
    return {
        userInfo: state['edubig/app'].userInfo,
        ebooks
    };
}

export default connect(_mapStateToProps)(Dashboard);