import React                from 'react';
import { connect }          from 'react-redux';
import {
    TouchableHighlight,
    View,
    Text
}                           from 'react-native';

class UserHeaderNotAuth extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{flex: 8}}>
                <TouchableHighlight
                    accessibilityLabel={'Tap to Join.'}
                    onPress={this.props.changeControl(1)}
                    >
                    <Text style={{lineHeight: 25}}>Đăng nhập</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    accessibilityLabel={'Tap to Join.'}
                    onPress={this.props.changeControl(2)}
                    >
                    <Text style={{lineHeight: 25}}>Đăng ký</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

function _mapStateToProps(state) {
    return {
    };
}

export default connect(_mapStateToProps)(UserHeaderNotAuth);