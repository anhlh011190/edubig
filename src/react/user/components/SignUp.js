import React                from 'react';
import { connect }          from 'react-redux';
import {
    TouchableHighlight,
    View,
    Text
}                           from 'react-native';
import {
    signUp
}                           from './../actions';
import {
    styles
}                           from './styles';

class SignUp extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: "",
            rePassword: ""
        }
    }

    _onUsernameChange = (username) => {
        this.setState({ username });
    }

    _onPasswordChange = (password) => {
        this.setState({ password });
    }

    _onRePasswordChange = (rePassword) => {
        this.setState({ rePassword });
    }

    signIn = () => {
        this.props.dispatch(login(this.state.username, this.state.password));
    }

    signUp = () => {
        if(this.state.password.length > 5 &&this.state.rePassword == this.state.password){
            this.props.dispatch(signUp(this.state.username, this.state.password));
        }else{
            alert("Thông tin đăng ký chưa hợp lệ!")
        }
    }

    render(){
        return(
            <View style={styles.loginForm}>
                <Text style={{lineHeight: 25, fontSize: 25, marginBottom: 10}}>Thông tin đăng ký</Text>
                <View style={styles.loginFormInput} >
                    <FontAwesome
                        name="envelope"
                        style={styles.loginFormInputIcon}
                    />
                    <TextInput
                        accessibilityLabel={'Tên đăng nhập'}
                        autoCapitalize='none'
                        autoCorrect={false}
                        autoFocus={false}
                        onChangeText={this._onUsernameChange}
                        placeholder={"Tên đăng nhập"}
                        placeholderTextColor='gray'
                        style={styles.loginFormInputIconText}
                        underlineColorAndroid='transparent'
                        value={this.state.username} />
                </View>
                <View style={styles.loginFormInput} >
                    <FontAwesome
                        name="lock"
                        style={styles.loginFormInputIcon}
                    />
                    <TextInput
                        accessibilityLabel={'Mật khẩu'}
                        autoCapitalize='none'
                        autoCorrect={false}
                        autoFocus={false}
                        onChangeText={this._onPasswordChange}
                        placeholder={"Mật khẩu"}
                        placeholderTextColor='gray'
                        style={styles.loginFormInputIconText}
                        secureTextEntry
                        underlineColorAndroid='transparent'
                        value={this.state.password} />
                </View>
                <View style={styles.loginFormInput} >
                    <FontAwesome
                        name="lock"
                        style={styles.loginFormInputIcon}
                    />
                    <TextInput
                        accessibilityLabel={'Nhập lại mật khẩu'}
                        autoCapitalize='none'
                        autoCorrect={false}
                        autoFocus={false}
                        onChangeText={this._onRePasswordChange}
                        placeholder={"Nhập lại mật khẩu"}
                        placeholderTextColor='gray'
                        style={styles.loginFormInputIconText}
                        secureTextEntry
                        underlineColorAndroid='transparent'
                        value={this.state.rePassword} />
                </View>
                <TouchableHighlight
                    accessibilityLabel={'Tap to Join.'}
                    onPress={this.signUp}
                    style={styles.loginBtn}>
                    <Text style={styles.loginBtnText}>Đăng ký</Text>
                </TouchableHighlight>
            </View>
        )
    }
}

function _mapStateToProps(state) {
    return {
        userInfo: state['edubig/app'].userInfo,
    };
}

export default connect(_mapStateToProps)(SignUp);