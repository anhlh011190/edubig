import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView,
    View,
    Text,
    StatusBar, TouchableHighlight
}                           from 'react-native';
import { 
    BottomBar, Header
}                           from './../../homepage';
import ImagePicker          from 'react-native-image-picker';
import { WebView }          from 'react-native-webview';
import { MathJax }          from './../../base/layout';

const defaultOptions = {
	messageStyle: 'none',
	extensions: [ 'tex2jax.js' ],
	jax: [ 'input/TeX', 'output/HTML-CSS' ],
	tex2jax: {
		inlineMath: [ ['$','$'], ['\\(','\\)'] ],
		displayMath: [ ['$$','$$'], ['\\[','\\]'] ],
		processEscapes: true,
	},
	TeX: {
		extensions: ['AMSmath.js','AMSsymbols.js','noErrors.js','noUndefined.js']
	}
};

class History extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    constructor(props){
        super(props);
    }

	wrapMathjax(content) {
		return `
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <script type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
                });
            </script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML" async></script>

			${content}
		`;
	}

    takeCamera = () => {
        var options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        };
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                console.log('User cancelled image picker',response);
            }
        });
    }

    render(){
        // const html = this.wrapMathjax("Phương trình $\\left\\{ \\begin{align} & x=-1+7t \\\\ & y=3-4t \\\\ \\end{align} \\right.$ có véc tơ pháp tuyến là:");
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5, flex: 1, backgroundColor: "red"}}>
                    <WebView
                        source={{ html }}
                        style={{
                          }}
                    />
                </View>
                <View style={{flex: 1}}>
                    <Text>ABC</Text>
                    <MathJax html={"Phương trình $\\left\\{ \\begin{align} & x=-1+7t \\\\ & y=3-4t \\\\ \\end{align} \\right.$ có véc tơ pháp tuyến là:"} />
                    <Text>ABC</Text>
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
    };
}

export default connect(_mapStateToProps)(History);