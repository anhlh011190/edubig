import {AsyncStorage}           from 'react-native';

export function get(url){
    return new Promise(function(resolve,reject){
        fetch( url , {
            method: "GET",
            headers: {
                'Access-Control-Allow-Origin':'*',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            mode: 'cors'
        }).then(function(response) {
            if(response.status === 200){
                return response.json();
            }else{
                resolve({success: false});
            }
        }).then(function(response){
            resolve(response);
        }).catch(err=>{
            reject({success: false, err});
        });
    });
}

export function post(url, param){
    return new Promise(function(resolve,reject){
        fetch(url, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify( param )
        }).then(function(response) {
            if(response.status === 200){
                return response.json();
            }else{
                return {};
            }
        }).then(function(response){
            resolve(response);
        }).catch(err=>{
            reject({success: false, err});
        });
    });
}

export function setAsyncStorage(key, data){
    return new Promise(function(resolve, reject){
        AsyncStorage.setItem(key, data);
    })
}

export function getAsyncStorage(key){
    return new Promise(function(resolve,reject){
        AsyncStorage.getItem(key)
        .then((rs)=> {
            resolve(rs);
        })
        .catch(err=>{
            reject({});
        });
    });
}