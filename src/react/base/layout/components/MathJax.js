import React                from 'react';
import { WebView }          from 'react-native-webview';
import {
    View
}                           from 'react-native';

class MathJax extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            height: props.height || 0
        };
    }

    onNavigationStateChange(navState) {
        this.setState({
            height: navState.title
        });
    }

	wrapMathjax(content) {
        content = content.replace(/(xxxx)/g, "\\")
        content = content.replace(/(\\ )/g, "\\\\")
		return `
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <script type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    tex2jax: {inlineMath: [['$','$']]}
                });
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML" async></script>
			${content}
		`;
	}

    render(){
        let {
            html,
            minHeight,
            style,
            autoHeight,
            scrollEnabled,
            ...props
        } = this.props;
        html = this.wrapMathjax(html);
        return(
            <View style={{width: "100%", minHeight}}>
                <WebView
                    {...props}
                    source={{ html }}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                />
            </View>
        )
    }
}

export default MathJax;