import React                from 'react';
import {
    Text, StyleSheet,
    TouchableHighlight
}                           from 'react-native';

function Btn({onPress, text, type}) {
    let styleBtn = styles.primary;
    switch (type) {
        case "primary":
            styleBtn = styles.primary;
            break;
        case "success":
            styleBtn = styles.success;
            break;
        case "danger":
            styleBtn = styles.danger;
            break;
        case "warning":
            styleBtn = styles.warning;
            break;
        default:
            styleBtn = styles.primary;
            break;
    }
    return(
        <TouchableHighlight 
            style={[styles.wrapper, styleBtn]}
            onPress={onPress}>
            <Text style={styles.text}>{text}</Text>
        </TouchableHighlight>
    )
}

export default Btn;

let styles = StyleSheet.create({
    wrapper: {borderRadius: 5,  backgroundColor: "blue", paddingHorizontal: 7, paddingVertical: 2, margin: 5},
    primary:{ backgroundColor: "#007bff"},
    success:{ backgroundColor: "#28a745"},
    danger:{ backgroundColor: "#dc3545"},
    warning:{ backgroundColor: "#ffc107"},
    text: {color: "#fff"}
});