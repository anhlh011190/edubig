
import { MiddlewareRegistry }   from '../redux';

MiddlewareRegistry.register(store => next => action => {
    switch (action.type) {
        case "JOIN_SOCKET":
            action.socket.on("USER_JOIN_NEW", data => {
                console.log("USER_JOIN_NEW",data)
            })
            action.socket.on("USER_ONLINE", data => {
                console.log("USER_ONLINE",data)
            })
            action.socket.on("USER_INFO", data => {
                console.log("USER_INFO",data)
            })
            return next(action);
        default:
            return next(action);
    }
});