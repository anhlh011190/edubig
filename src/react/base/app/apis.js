import { post, get } from "./../portal";

export function saveLog(type, data){
    let params = {
        type,
        data: JSON.stringify(data)
    }
    return post("https://api.edubig.vn/api/v2/log", params);
}
export function checkUpdateApp(system, version){
    return get("https://api.edubig.vn/api/v2/checkupdateapp?system="+system+"&version="+version);
}