import {
    getDeviceInformation
}                           from './../devices';
import {
    Alert, Linking
}                           from 'react-native';
import {
    saveLog,
    checkUpdateApp as checkUpdateAppApi
}                           from './apis';

export function changeScreen(screen) {
    return {
        type: "CHANGE_SCREEN",
        screen
    }
}

export function joinSocket(socket) {
    return {
        type: "JOIN_SOCKET",
        socket
    }
}

export function changeUserInfo(userInfo){
    return {
        type: "CHANGE_USER_INFO",
        userInfo
    }
}

export function signOut(){
    return {
        type: "SIGN_OUT"
    }
}

export function checkUpdateApp(){
    let deviceInfo = getDeviceInformation();

    checkUpdateAppApi(deviceInfo.systemName, deviceInfo.appVersion).then(rsCheckUpdateApp => {
        if(rsCheckUpdateApp.result){
            Alert.alert(
                "Thông báo!",
                "Hiện tại đã có bản cập nhật app mới, vui lòng nhấn tải về!",
                [{text:"Tải về",onPress:()=>{
                    Linking.openURL("https://play.google.com/store/apps/details?id=vn.com.edubig")}
                }],
                { cancelable: true }
            )
        }
    })
    return {
        type: "ETC"
    }
}

export function saveDeviceInfo() {
    let deviceInfo = getDeviceInformation();
    return function (dispatch, getState) {
        let userInfo = {uniqueID: deviceInfo.uniqueID};
        dispatch(changeUserInfo(userInfo));

        saveLog("DEVICE", deviceInfo).then(rsSaveLog => {
            return {
                type: "ETC"
            }
        }).catch(err=>{
            console.log(err)
        })
        return {
            type: "ETC"
        }
    }
}