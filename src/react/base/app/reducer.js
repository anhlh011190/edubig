import { ReducerRegistry }  from '../redux';
let stateDefault = {
    screen        : "Homepage",
    userInfo: { isLogin: false },
    socket: null
}

ReducerRegistry.register('edubig/app', (state = stateDefault, action) => {
    switch (action.type) {
        case "SIGN_OUT":
            return {
                ...state,
                userInfo: { isLogin: false }
            }
        case "CHANGE_USER_INFO":
            return {
                ...state,
                userInfo: {...state.userInfo, ...action.userInfo}
            }
        case "CHANGE_SCREEN":
            return{
                ...state,
                screen: action.screen
            }
        case "JOIN_SOCKET":
            return{
                ...state,
                socket: action.socket
            }
        default:
            return state;
    }
});