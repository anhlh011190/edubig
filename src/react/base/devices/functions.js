import DeviceInfo from "react-native-device-info";

export function getDeviceInformation() {
    let pin, ipAddress;
    DeviceInfo.getBatteryLevel().then(batteryLevel => {
        pin = batteryLevel;
    });
    DeviceInfo.getIPAddress().then(ip => {
        ipAddress = ip;
    });
    let data = {
        "brand"            : DeviceInfo.getBrand(),
        "appVersion"       : DeviceInfo.getVersion(),
        "systemVersion"    : DeviceInfo.getSystemVersion(),
        "deviceName"       : DeviceInfo.getDeviceName(),
        "deviceId"         : DeviceInfo.getDeviceId(),
        "pin"              : pin,
        "carrier"          : DeviceInfo.getCarrier(),
        "freeDisk"         : DeviceInfo.getFreeDiskStorage(),
        "ipAddress"        : ipAddress,
        "maxMemory"        : DeviceInfo.getMaxMemory(),
        "systemName"       : DeviceInfo.getSystemName(),
        "timeZone"         : DeviceInfo.getTimezone(),
        "uniqueID"         : DeviceInfo.getUniqueID()
    };
    return data;
}
