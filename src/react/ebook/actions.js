import { 
    getEbooks,
    countEbook
}                           from './apis';

export function setEbooks(ebooks, data, page, type = "ALL", q="") {
    return {
        type: "SET_EBOOK",
        typeLevel2 : type,
        page,
        ebooks,
        data,
        q
    };
}

export function changeTotalPage(totalPage){
    return {
        type: "CHANGE_TOTAL_PAGE_EBOOK",
        typeLevel2 : "ALL",
        totalPage
    };
}

export function setLogs(id, page, logs, title, source){
    return {
        type: "UPDATE_LOGS",
        id,
        page,
        logs,
        title,
        source
    }
}

export function updateLogs(id, page, title, source){
    return function (dispatch, getState) {
        let logs  = getState()['edubig/ebook'].logs;
        dispatch(setLogs(id, page, logs, title, source));
    }
}

export function initEbooksStorage(ebooks){
    return {
        type : "INIT_EBOOK",
        ebooks
    }
}

export function initLogsStorage(logs){
    return {
        type : "INIT_LOGS",
        logs
    }
}

export function getEbooksbyPage(page,type="ALL",q=""){
    return function (dispatch, getState) {
        let ebooks  = getState()['edubig/ebook'];
        dispatch(initEbooksStorage({page}));
        getEbooks(page, q).then((rsEbooks) => {
            if(rsEbooks.success){
                dispatch(setEbooks(rsEbooks.result, ebooks, page, type, q));
            }
        })
    }
}

export function initEbooks() {
    return function (dispatch, getState) {
        let ebooks  = getState()['edubig/ebook'];
        let page    = 1;
        getEbooks().then((rsEbooks) => {
            if(rsEbooks.success){
                dispatch(setEbooks(rsEbooks.result, ebooks, page));
            }
        })
        countEbook().then((rsCountEbook) => {
            if(rsCountEbook.success){
                dispatch(changeTotalPage(rsCountEbook.result));
            }
        })
    }
}