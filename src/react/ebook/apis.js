import { get }                  from './../base/portal';
import { config }               from './config';

export function getEbooks(page=1, q=""){
    return get("https://api.edubig.vn/api/v2/ebooks?limit="+config.totalPage+"&page="+page+"&q="+q);
}

export function countEbook(){
    return get("https://api.edubig.vn/api/v2/ebook/count");
}