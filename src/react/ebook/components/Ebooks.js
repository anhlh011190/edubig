import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView, TextInput,
    Text, View, TouchableHighlight,
    StatusBar, ScrollView,
    Dimensions
}                           from 'react-native';
import { 
    BottomBar, Header
}                           from './../../homepage';
import ModalSelector        from 'react-native-modal-selector';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';
import {
    initEbooks, getEbooksbyPage,
    initEbooksStorage
}                           from './../actions';
import { config }           from './../config';
import {
    changeScreen
}                           from './../../base/app';

class Ebooks extends React.Component{
    constructor(props){
        super(props);
    }
    static navigationOptions = {
        headerTitle: <Header />
    };
    
    UNSAFE_componentWillMount(){
        this.props.dispatch(initEbooks());
    }

    changePage = (page) => {
        this.props.dispatch(getEbooksbyPage(page.key));
    }

    onCancelSearch = () => {
        this.props.dispatch(initEbooksStorage({q:""}));
    }

    onPress = (screen,data) => () => {
        this.props.dispatch(changeScreen("Ebooks"));
        this.props.navigation.navigate(screen, data);
    }

    render(){
        let ebooks;
        ebooks = this.props.ebooks.find(d => d.page == this.props.page);
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5}}>
                    <View style={{height: 30, flexDirection: "row"}}>
                        <View style={{flex: 6}}>
                            <Text style={{textTransform:"uppercase"}}>Danh sách tài liệu PDF - Doc</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: "row", justifyContent:"flex-end"}}>
                            <Text style={{lineHeight: 30, marginRight: 5}}>Trang</Text>
                            <ModalSelector
                                    data={this.props.listPage}
                                    initValue=""
                                    onChange={this.changePage}
                                    style={{width: 50, height: 30, borderWidth: 1, borderRadius: 15}}
                                >
                                <TextInput
                                    style={{ padding:0, width: 50, height:28, fontSize: 12, textAlign:'center', color: '#686'}}
                                    editable={false}
                                    underlineColorAndroid="transparent"
                                    placeholder={""}
                                    value={this.props.page+"/"+this.props.totalPage} />
                            </ModalSelector>
                        </View>
                    </View>
                    {
                        this.props.q != "" &&
                        <View style={{flexDirection:"row"}}>
                            <Text>Kết quả tìm kiếm: {this.props.q}</Text>
                            <TouchableHighlight 
                                onPress={this.onCancelSearch}>
                                <FontAwesome style={{fontSize: 18, marginRight: 10, marginTop: 2}} name='times' />
                            </TouchableHighlight>
                        </View>
                    }
                    <ScrollView style={{width: (Dimensions.get('window').width-20), height: (Dimensions.get('window').height-160)}}>
                        {
                            ebooks != undefined &&
                            ebooks.data.map((e,index)=>{
                                return(
                                    <TouchableHighlight onPress={this.onPress("Ebook",e)} key={index}>
                                        <View style={{flexDirection:"row", paddingHorizontal: 10, width: "100%", marginTop: 5}}>
                                            <FontAwesome style={{fontSize: 10, marginRight: 10, marginTop: 5}} name='circle' />
                                            <Text style={{color: "#000"}}>{e.title}</Text>
                                        </View>
                                    </TouchableHighlight>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    let listPage = [];
    let totalPage = 1;
    let ebooks = [];
    if (state['edubig/ebook'].type == "ALL" || state['edubig/ebook'].q == ""){
        ebooks = state['edubig/ebook'].ebooks.homepage;
        totalPage = Math.ceil(state['edubig/ebook'].totalPage.all/config.totalPage);
        for (var i = 0 ;i <= state['edubig/ebook'].totalPage.all/config.totalPage; i++) {
            if (i == state['edubig/ebook'].page-1){
                listPage.push({key:i+1, label:i+1+'', section:true});
            }else {
                listPage.push({key:i+1, label:i+1+''});
            }
        }
    }else{
        ebooks = state['edubig/ebook'].ebooks.search;
        totalPage = Math.ceil(state['edubig/ebook'].totalPage.search/config.totalPage);
        for (var j = 0 ;j <= state['edubig/ebook'].totalPage.search/config.totalPage; j++) {
            if (j == state['edubig/ebook'].page-1){
                listPage.push({key:j+1, label:j+1+'', section:true});
            }else {
                listPage.push({key:j+1, label:j+1+''});
            }
        }
    }

    return {
        ebooks,
        q: state['edubig/ebook'].q,
        page  : state['edubig/ebook'].page,
        listPage,
        totalPage
    };
}

export default connect(_mapStateToProps)(Ebooks);