import { ReducerRegistry } from '../base/redux';
let stateDefault = {
    ebooks: {
        homepage: [],
        search: []
    },
    page: 1,
    totalPage: {
        all: 1,
        search: 1
    },
    logs: [],
    q:"",
    search: false,
    type: "ALL"
}

ReducerRegistry.register('edubig/ebook', (state = stateDefault, action) => {
    switch (action.type) {
        case "UPDATE_LOGS":
            let time = Math.round(new Date().getTime() / 1000);
            if(state.logs.findIndex(log => log.id == action.id) != -1){
                return {
                    ...state,
                    logs: state.logs.map(log => {
                        return log.id == action.id ? {...log, page: action.page, time, source: action.source} : log
                    })
                }
            }else{
                return {
                    ...state,
                    logs: [...state.logs, {id: action.id, page: action.page, title: action.title, time, source: action.source}]
                }
            }
        case "CHANGE_TOTAL_PAGE_EBOOK":
            switch(action.typeLevel2){
                case "ALL":
                    return {
                        ...state,
                        totalPage: {
                            ...state.totalPage,
                            all: action.totalPage
                        }
                    }
            }
            return {
                ...state,
                totalPage: action.totalPage
            }
        case "INIT_LOGS":
            return {
                ...state,
                logs: action.logs
            }
        case "INIT_EBOOK":
            return {
                ...state,
                ...action.ebooks
            }
        case "SET_EBOOK":
            switch(action.typeLevel2){
                case "ALL":
                    if(state.ebooks.homepage.findIndex(e => e.page == action.page) != -1){
                        return{
                            ...state,
                            type: action.typeLevel2,
                            q: action.q,
                            page: action.page,
                            ebooks: {
                                ...state.ebooks,
                                homepage: state.ebooks.homepage.map(ebook => {
                                    return ebook.page == action.page ? {page: ebook.page, data: action.ebooks} : ebook
                                })
                            }
                        }
                    }else{
                        return{
                            ...state,
                            type: action.typeLevel2,
                            q: action.q,
                            page: action.page,
                            ebooks: {
                                ...state.ebooks,
                                homepage: [...state.ebooks.homepage, {page: action.page, data: action.ebooks}]
                            }
                        }
                    }
                case "SEARCH":
                        if(state.ebooks.search.findIndex(e => e.page == action.page) != -1){
                            return{
                                ...state,
                                type: action.typeLevel2,
                                q: action.q,
                                page: action.page,
                                ebooks: {
                                    ...state.ebooks,
                                    search: state.ebooks.search.map(ebook => {
                                        return ebook.page == action.page ? {page: ebook.page, data: action.ebooks} : ebook
                                    })
                                }
                            }
                        }else{
                            return{
                                ...state,
                                type: action.typeLevel2,
                                q: action.q,
                                page: action.page,
                                ebooks: {
                                    ...state.ebooks,
                                    search: [...state.ebooks.search, {page: action.page, data: action.ebooks}]
                                }
                            }
                        }
                default:
                    return state;
            }
        default:
            return state;
    }
});