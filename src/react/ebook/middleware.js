
import { MiddlewareRegistry }   from '../base/redux';
import {
    setAsyncStorage
}                               from '../base/portal';

let mergeData = (type, ebooks, data, page) => {
    return new Promise(function(resolve, reject){
        switch (type) {
            case "ALL":
                if(data.ebooks.homepage.findIndex(e => e.page == page) != -1){
                    resolve({
                        ...data,
                        ebooks:{
                            ...data.ebooks,
                            homepage: data.ebooks.homepage.map(ebook => {
                                return ebook.page == page ? {page: ebook.page, data: ebooks} : ebook
                            })
                        }
                    });
                }else{
                    resolve({
                        ...data,
                        ebooks:{
                            ...data.ebooks,
                            homepage: [...data.ebooks.homepage, {page, data: ebooks}]
                        }
                    });
                }
        }
    })
}

let mergeDataLogs = (id, page, logs, title, source) => {
    return new Promise(function(resolve, reject){
        if(logs.findIndex(log => log.id == id) != -1){
            resolve(
                logs.map(log => {
                    return log.id == id ? {...log, page, source} : log
                })
            );
        }else{
            resolve(
                [...logs, {id, page, title, source}]
            );
        }
    })
}

MiddlewareRegistry.register(store => next => action => {
    switch (action.type) {
        case "UPDATE_LOGS":
            mergeDataLogs(action.id, action.page, action.logs, action.title, action.source)
            .then(data => {
                setAsyncStorage("LOGS",JSON.stringify(data));
            })
            return next(action);
        case "SET_EBOOK":
            mergeData(action.typeLevel2, action.ebooks, action.data, action.page)
            .then(data => {
                setAsyncStorage("EBOOK",JSON.stringify(data));
            })
            return next(action);
        default:
            return next(action);
    }
});