import { ReducerRegistry }  from '../base/redux';
let stateDefault = {
}

ReducerRegistry.register('edubig/homepage', (state = stateDefault, action) => {
    switch (action.type) {
        default:
            return state;
    }
});