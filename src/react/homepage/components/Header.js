import React                from 'react';
import { connect }          from 'react-redux';
import {
    StyleSheet, View, 
    TextInput, Text,
    TouchableHighlight
}                           from 'react-native';
import {
    getEbooksbyPage
}                           from './../../ebook/actions';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';

class Header extends React.Component{
    constructor(props){
    super(props);
        this.state = {
            q: ""
        }
    }

    onChangeText = (q) => {
        this.setState({ q });
    }

    onSearch = () => {
        this.props.dispatch(getEbooksbyPage(1, "SEARCH", this.state.q));
    }

    render(){
        return(
            <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:9}}>
                    {
                        this.props.screen == "Ebooks" ?
                        <View style={styles.loginFormInput} >
                            <TextInput
                                accessibilityLabel={'Mật khẩu'}
                                autoCapitalize='none'
                                autoCorrect={false}
                                autoFocus={false}
                                onChangeText={this.onChangeText}
                                placeholder={"Tìm kiếm..."}
                                placeholderTextColor='gray'
                                style={styles.loginFormInputIconText}
                                underlineColorAndroid='transparent'
                                value={this.state.q} />
                            <TouchableHighlight
                                accessibilityLabel={'Tap to Join.'}
                                onPress={this.onSearch}
                                style={{paddingTop:5, paddingRight: 5}}
                                >
                                <FontAwesome
                                    name="search"
                                    style={styles.loginFormInputIcon}
                                />
                            </TouchableHighlight>
                        </View>
                        :
                        <Text style={{textAlign:"center"}}>{this.props.title}</Text>
                    }
                </View>
                <View style={{flex:1}}></View>
            </View>
        )
    }
}

function _mapStateToProps(state) {
    let title = "";
    switch (state['edubig/app'].screen) {
        case "Ebook":
            title = "Chi tiết tài liệu";
            break;
        case "Quest":
            title = "Luyện trắc nghiệm";
            break;
        case "Exams":
            title = "Danh sách đề thi";
            break;
        case "Exam":
            title = "Chi tiết đề thi";
            break;
        case "Homepage":
            title = "Trang chủ";
            break;
        case "Ebooks":
            title = "Danh sách tài liệu";
            break;
        default:
            title = state['edubig/app'].screen;
            break;
    }
    return {
        screen: state['edubig/app'].screen,
        title
    };
}

export default connect(_mapStateToProps)(Header);

let styles = StyleSheet.create({
    loginFormInput: {
        flexDirection: 'row',
        alignSelf:"flex-end",
        width: 250,
        height: 35,
        backgroundColor: 'rgba(230, 255, 255, .3)',
        borderRadius: 18,
        marginTop: 5,
        alignItems:'center',
        paddingLeft: 10
    },
    loginFormInputIcon: {
        flex: 1.5,
        marginLeft: 12,
        color: '#000',
        fontSize: 20
    },
    loginFormInputIconText: {
        color: '#000',
        fontSize: 11,
        flex: 8.5
    }
});