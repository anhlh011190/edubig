import React                from 'react';
import { connect }          from 'react-redux';
import {
    changeScreen
}                           from './../../base/app';
import {
    View, StyleSheet,
    Text, TouchableHighlight
}                           from 'react-native';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';

function Item({icon, text, active, onPress}){
    return(
        <TouchableHighlight onPress={onPress} style={styles.item}>
            <View style={styles.item}>
                <FontAwesome style={active ? styles.itemIconActive: styles.itemIcon} name = {icon} />
                <Text style={active ? styles.itemTextActive: styles.itemText}>{text}</Text>
            </View>
        </TouchableHighlight>
    )
}

class BottomBar extends React.Component{
    constructor(props){
        super(props);
    }

    onPress = (screen) => () => {
        this.props.dispatch(changeScreen(screen));
        this.props.navigation.navigate(screen);
    }
    
    render(){
        return(
            <View style={styles.wrapper}>
                <Item icon={"home"} text={"Trang chủ"} active={this.props.screen == "Homepage" ? true : false} onPress={this.onPress("Homepage")}/>
                <Item icon={"check-square"} text={"Đề thi"} active={this.props.screen == "Exams" ? true : false} onPress={this.onPress("Exams")}/>
                <Item icon={"envelope"} text={"Hộp thư"} active={this.props.screen == "Inbox" ? true : false}/>
                <Item icon={"file"} text={"PDF"} active={this.props.screen == "Ebooks" ? true : false} onPress={this.onPress("Ebooks")}/>
                <Item icon={"user"} text={"Tài khoản"} active={this.props.screen == "Dashboard" ? true : false} onPress={this.onPress("Dashboard")}/>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    wrapper:{
        flexDirection:"row",position:"absolute", bottom: 0, backgroundColor: "#f2f2f2", paddingBottom: 15, paddingTop: 5
    },
    item:{
        flex:1,alignItems:"center"
    },
    itemIcon:{
        fontSize: 20, marginBottom: 5
    },
    itemIconActive:{
        fontSize: 20, color:"#00b300", marginBottom: 5
    },
    itemText:{
        color: "#000"
    },
    itemTextActive:{
        color:"#00b300"
    }
});

function _mapStateToProps(state) {
    return {
        screen: state['edubig/app'].screen
    };
}

export default connect(_mapStateToProps)(BottomBar);