import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView,
    View, TouchableHighlight,
    Text, StyleSheet,
    StatusBar
}                           from 'react-native';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';
import BottomBar            from './BottomBar';
import {
    changeScreen,
    checkUpdateApp
}                           from './../../base/app';

function Item({icon, text, onPress}){
    return(
        <TouchableHighlight onPress={onPress} style={styles.item}>
            <View style={styles.itemWrapper}>
                <FontAwesome style={styles.itemIcon} name={icon} />
                <Text style={styles.itemText}>{text}</Text>
            </View>
        </TouchableHighlight>
    )
}
class Homepage extends React.Component{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props);
    }

    UNSAFE_componentWillMount(){
        this.props.dispatch(checkUpdateApp());
    }

    onPress = (screen, title, data) => () => {
        this.props.dispatch(changeScreen(title));
        this.props.navigation.navigate(screen, data);
    }
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{backgroundColor:"#00b386", alignItems:"center", height:150}}>
                    <Text style={{color: "#fff", marginTop:50}}>Chào {this.props.userInfo.isLogin ? (this.props.userInfo.fullname ? this.props.userInfo.fullname: this.props.userInfo.username): ""}!</Text>
                </View>
                <View style={{marginTop:15, flexDirection:"row"}}>
                    <Item icon={"calculator"} text={"Toán học"} onPress={this.onPress("Theory", "Lý thuyết Toán học", {idCategory: "edubig79e857fafdfbc9166d4c37f8dca7e386"})} />
                    {/* <Item icon={"user-md"} text={"Vật lý"} onPress={this.onPress("Theory", "Lý thuyết Vật lý", {idCategory: "edubig0bab494819ec42753a7fc1bbfada1b60"})} /> */}
                    {/* <Item icon={"user"} text={"Hoá học"} onPress={this.onPress("Theory", "Lý thuyết Hoá học", {idCategory: "edubigfdc1719a01f366673c96c097522c23cd"})} /> */}
                    {/* <Item icon={"user"} text={"Sinh học"} onPress={this.onPress("Theory", "Lý thuyết Sinh học", {idCategory: "edubig2750d3666ffff1f57b1a2badbeb4d529"})} /> */}
                </View>
                {/* <View style={{flexDirection:"row",marginTop:15}}>
                    <Item icon={"calculator"} text={"Tiếng Anh"} onPress={this.onPress("Theory", "Lý thuyết Tiếng anh", {idCategory: "edubig8f1ead032c3580257a5971edf4eafe51"})} />
                    <Item icon={"user-md"} text={"Địa lý"} onPress={this.onPress("Theory", "Lý thuyết Địa lý", {idCategory: "edubigd0a20bcdc9c89607f0b33d0e01a32ff9"})} />
                    <Item icon={"history"} text={"Lịch sử"} onPress={this.onPress("Theory", "Lý thuyết Lịch sử", {idCategory: "edubig3061f54407a0a17fad476b866c21a33a"})} />
                    <Item icon={"user"} text={"Ngữ Văn"} onPress={this.onPress("Theory", "Lý thuyết Ngữ văn", {idCategory: "edubigc56c5450fbad2fe03c1c6dae4f28b0f7"})} />
                </View> */}
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

let styles = StyleSheet.create({
    item: {flex:1},
    itemWrapper: {alignItems:"center"},
    itemIcon: {fontSize: 20},
    itemText: {color: "#000"}
});

function _mapStateToProps(state) {
    return {
        userInfo: state['edubig/app'].userInfo
    };
}

export default connect(_mapStateToProps)(Homepage);