import React                from 'react';
import { connect }          from 'react-redux';
import {
    Text, SafeAreaView,
    StatusBar, View, Dimensions,
    ScrollView, TouchableHighlight
}                           from 'react-native';
import {
    getTheories
}                           from './../actions';
import { 
    BottomBar, Header
}                           from './../../homepage';
import {
    changeScreen
}                           from './../../base/app';
import {
    Btn
}                           from './../../base/layout';

class Theory extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    componentWillMount(){
        this.props.dispatch(getTheories(this.props.navigation.getParam('idCategory')));
    }

    onPress = (data) => () => {
        this.props.navigation.navigate("TheoryDetail", data);
    }

    onPressQuest = data => () => {
        this.props.dispatch(changeScreen("Quest"));
        this.props.navigation.navigate("Quest", data);
    }

    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5}}>
                    <ScrollView style={{height: (Dimensions.get('window').height-130)}}>
                        {
                            this.props.theries.map((theory,index)=>{
                                return(
                                    <View key={index} style={{margin: 5}}>
                                        <Text style={{fontWeight: "bold", color: "#3385ff"}}>{theory.title}</Text>
                                        {
                                            theory.data.map((theoryL2, indexL2)=>{
                                                return(
                                                    <View key={indexL2}>
                                                        <Text style={{marginLeft:15, fontWeight: "bold"}}>- {theoryL2.title}</Text>
                                                        <View>
                                                            {
                                                                theoryL2.data.map((theoryL3, indexL3)=>{
                                                                    return(
                                                                        <View key={indexL3} >
                                                                            <Text style={{marginLeft:30}}>+ {theoryL3.title}</Text>
                                                                            <View style={{flexDirection: "row", justifyContent: "center"}}>
                                                                                {
                                                                                    theoryL3.drive && <Btn type={"success"} text={"Lý thuyết"} onPress={this.onPress(theoryL3)} />
                                                                                }
                                                                                <Btn text={"Bài tập"} onPress={this.onPressQuest(theoryL3)} />
                                                                            </View>
                                                                        </View>
                                                                    )
                                                                })
                                                            }
                                                        </View>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
        theries : state['edubig/theory'].theories
    };
}

export default connect(_mapStateToProps)(Theory);