import React                from 'react';
import { connect }          from 'react-redux';
import {
    Image, SafeAreaView, View,
    StatusBar, Text, Dimensions,
    ScrollView
}                           from 'react-native';
import {
    getQuestInfo,
    checkQuest
}                           from './../actions';
import { 
    BottomBar, Header
}                           from './../../homepage';
import {
    CheckBox
}                           from 'react-native-elements';
import {
    Btn, MathJax
}                           from './../../base/layout';

class Quest extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    constructor(props){
        super(props);
        this.state = {
            plan: ""
        }
    }

    componentWillMount(){
        this.props.dispatch(getQuestInfo(this.props.navigation.getParam('id')));
    }

    choicePlan = plan => () => {
        this.setState({plan})
    }

    checkQuest = () => {
        this.props.dispatch(checkQuest(this.state.plan, this.props.questInfo, this.props.navigation.getParam('id')));
        this.setState({plan:""});
    }

    nextQuest = () => {
        this.setState({plan:""})
        this.props.dispatch(getQuestInfo(this.props.navigation.getParam('id')));
    }

    render(){
        let content;
        if(this.props.questInfo && this.props.questInfo.id && this.props.questInfo.content){
            let contentJson = JSON.parse(this.props.questInfo.content);
            content = `
                        ${contentJson.content}<br/>
                        A. ${contentJson.plana}<br/>
                        B. ${contentJson.planb}<br/>
                        C. ${contentJson.planc}<br/>
                        D. ${contentJson.pland}
                        `;
        }  
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{flex: 10, padding: 5, marginTop: 10}}>
                    <Text style={{fontWeight: "bold", marginBottom: 5}}>{this.props.navigation.getParam('title')}</Text>
                    {
                    (this.props.questInfo && this.props.questInfo.id) &&
                    <View style={{height: (Dimensions.get('window').height-160), flex: 1}}>
                        <Text>Mã: {this.props.questInfo.id}</Text>
                        {
                            this.props.questInfo.type == 1 &&
                            <MathJax minHeight={Dimensions.get('window').height-290} autoHeight={true} html={content} />
                        }
                        {
                            this.props.questInfo.type == null &&
                            <Image
                                key={this.props.questInfo.id}
                                style={{resizeMode: "contain", width: (Dimensions.get('window').width-20), height: (Dimensions.get('window').height-290),marginTop: 10}}
                                source={{uri: this.props.questInfo.source}}
                            />
                        }
                        <View style={{flexDirection:"row", height: 60}}>
                            <View style={{flex: 1}}>
                                <CheckBox 
                                    title="A"
                                    checked={this.state.plan == "A"}
                                    onPress={this.choicePlan("A")}
                                />
                            </View>
                            <View style={{flex: 1}}>
                                <CheckBox 
                                    title="B"
                                    checked={this.state.plan == "B"}
                                    onPress={this.choicePlan("B")}
                                />
                            </View>
                            <View style={{flex: 1}}>
                                <CheckBox 
                                    title="C"
                                    checked={this.state.plan == "C"}
                                    onPress={this.choicePlan("C")}
                                />
                            </View>
                            <View style={{flex: 1}}>
                                <CheckBox 
                                    title="D"
                                    checked={this.state.plan == "D"}
                                    onPress={this.choicePlan("D")}
                                />
                            </View>
                        </View>
                        <View style={{flexDirection: "row", justifyContent: "center"}}>
                            <Btn text={"Kiểm tra đáp án"} type="primary" onPress={this.checkQuest}/>
                            <Btn text={"Câu tiếp theo"} type="success" onPress={this.nextQuest}/>
                        </View>
                    </View>
                    }
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
        questInfo: state['edubig/theory'].questInfo
    };
}

export default connect(_mapStateToProps)(Quest);