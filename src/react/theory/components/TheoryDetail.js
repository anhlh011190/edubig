import React                from 'react';
import { connect }          from 'react-redux';
import {
    Text, SafeAreaView,
    StatusBar, View, 
    Dimensions
}                           from 'react-native';
import Pdf                  from 'react-native-pdf';
import { 
    BottomBar, Header
}                           from './../../homepage';

class TheoryDetail extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    render(){
        const source = {uri:"https://drive.google.com/uc?export=download&id="+this.props.navigation.getParam('drive'),cache:true};
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5, flex: 10}}>
                    <Text style={{textTransform:"uppercase"}}>{this.props.navigation.getParam('title')}</Text>
                    <Pdf
                        source={source}
                        onLoadComplete={(numberOfPages,filePath)=>{
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page,numberOfPages)=>{
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        style={{
                            flex:1,
                            width:(Dimensions.get('window').width-10),
                            height: "60%"
                        }}
                    />
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
        theries : state['edubig/theory'].theories
    };
}

export default connect(_mapStateToProps)(TheoryDetail);