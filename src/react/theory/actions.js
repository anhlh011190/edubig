import { 
    getTheories as getTheoriesApi,
    getQuest
}                                   from './apis';
import {
    saveLog
}                                   from './../base/app/apis';

import {
    Alert
}                           from 'react-native';

export function setTheories(theories) {
    return {
        type: "SET_THEORY",
        theories
    };
}

export function setQuestInfo(questInfo) {
    return {
        type: "SET_QUEST",
        questInfo
    };
}

export function getTheories(idCategory) {
    return function (dispatch, getState) {
        getTheoriesApi(idCategory).then((rsTheories) => {
            dispatch(setTheories(rsTheories.result));
        })
    }
}

export function getQuestInfo(idThematic) {
    return function (dispatch, getState) {
        getQuest(idThematic).then((rsQuest) => {
            if(rsQuest.success){
                dispatch(setQuestInfo(rsQuest.result));
            }
        })
    }
}

export function checkQuest(plan, questInfo, id){
    return function (dispatch, getState) {
        if(plan == ""){
            alert("Bạn chưa chọn đáp án!");
        }else{
            let userInfo =  getState()['edubig/app'].userInfo;
            let questChoiceInfo = {
                userId: userInfo.isLogin ? userInfo.id : userInfo.uniqueID,
                questId: questInfo.id,
                plan,
                answer: questInfo.answer
            }
            saveLog("QUEST", questChoiceInfo).then(rsSaveLog => {
                return {
                    type: "ETC"
                }
            }).catch(err=>{
                console.log(err)
            })
            if(questInfo.answer){
                if(questInfo.answer == plan){
                    Alert.alert(
                        "Thông báo!",
                        "Bạn đã trả lời chính xác. Đáp án đúng "+questInfo.answer,
                        [{text:"Câu tiếp theo",onPress:()=>{dispatch(getQuestInfo(id));} }],
                        { cancelable: true }
                    )
                }else{
                    Alert.alert(
                        "Thông báo!",
                        "Bạn đã trả lời sai. Đáp án đúng "+questInfo.answer,
                        [{text:"Câu tiếp theo",onPress:()=>{dispatch(getQuestInfo(id));} }],
                        { cancelable: true }
                    )
                }
            }else{
                Alert.alert(
                    "Thông báo!",
                    "Câu hỏi này chưa có đáp án!",
                    [{text:"Câu tiếp theo",onPress:()=>{dispatch(getQuestInfo(id));} }],
                    { cancelable: true }
                )
            }
        }
    }
}