import { ReducerRegistry } from '../base/redux';
let stateDefault = {
    theories: [],
    questInfo: {}
}

ReducerRegistry.register('edubig/theory', (state = stateDefault, action) => {
    switch (action.type) {
        case "SET_QUEST":
            return {
                ...state,
                questInfo: action.questInfo
            }
        case "SET_THEORY":
            return{
                ...state,
                theories: action.theories
            }
        default:
            return state;
    }
});