import { get } from "./../base/portal";

export function getTheories(idCategory, idClass="edubig248b9d8a29a82760d55b399a878be6fc"){
    return get("https://api.edubig.vn/api/v2/thematics?idCategory="+idCategory+"&idClass="+idClass);
}

export function getQuest(idThematic){
    return get("https://api.edubig.vn/api/v2/quest/random?idthematic="+idThematic+"&type=1");
}