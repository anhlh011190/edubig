import { 
    getExams,
    countExam
}               from './apis';

export function setExams(exams, data, page) {
    return {
        type: "SET_EXAM",
        typeLevel2 : "HOMEPAGE",
        page,
        data,
        exams
    };
}

export function changeTotalPage(totalPage){
    return {
        type: "CHANGE_TOTAL_PAGE_EXAM",
        totalPage
    };
}

export function initExamsStorage(exams){
    return {
        type : "INIT_EXAM",
        exams
    }
}

export function getExamsbyPage(page){
    return function (dispatch, getState) {
        let exams  = getState()['edubig/exam'];
        dispatch(initExamsStorage({page}))
        getExams(page).then((rsExams) => {
            if(rsExams.success){
                dispatch(setExams(rsExams.result, exams, page));
            }
        })
    }
}

export function initExams() {
    return function (dispatch, getState) {
        let exams  = getState()['edubig/exam'];
        let page    = 1;
        getExams(page).then((rsExams) => {
            if(rsExams.success){
                dispatch(setExams(rsExams.result, exams, page));
            }
        })
        countExam().then((rsCountExam) => {
            if(rsCountExam.success){
                dispatch(changeTotalPage(rsCountExam.result));
            }
        })
    }
}