import React                from 'react';
import { connect }          from 'react-redux';
import Pdf                  from 'react-native-pdf';
import {
    SafeAreaView,
    Text, View,
    StatusBar, Dimensions
}                           from 'react-native';
import { 
    BottomBar, Header
}                           from './../../homepage';
import {
    updateLogs
}                           from './../../ebook';

class Exam extends React.Component{
    static navigationOptions = {
        headerTitle: <Header />
    };

    constructor(props){
        super(props);
    }

    onLoadComplete = (numberOfPages,filePath) => {

    }

    onPageChanged = (page,numberOfPages) => {
        if(page != 1){
            this.props.dispatch(updateLogs(this.props.navigation.getParam('id'), page, this.props.navigation.getParam('title'), this.props.navigation.getParam('source')));
        }
    }

    render(){
        let source;
        if(this.props.navigation.getParam('source')){
            source = {uri: this.props.navigation.getParam('source'),cache:true};
        }else{
            source = {uri: "https://drive.google.com/uc?export=download&id="+this.props.navigation.getParam('drive'), cache:true};
        }
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5, flex: 10}}>
                    <Text style={{textTransform:"uppercase"}}>{this.props.navigation.getParam('title')}</Text>
                    <Pdf
                        source={source}
                        onLoadComplete={this.onLoadComplete}
                        onPageChanged ={this.onPageChanged}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        style={{
                            flex:1,
                            width:(Dimensions.get('window').width-10),
                            height: "60%"
                        }}
                    />
                </View>
                <BottomBar style={{flex:1}} {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    return {
    };
}

export default connect(_mapStateToProps)(Exam);