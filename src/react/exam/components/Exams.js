import React                from 'react';
import { connect }          from 'react-redux';
import {
    SafeAreaView, TextInput,
    Text, View, TouchableHighlight,
    StatusBar, ScrollView,
    Dimensions
}                           from 'react-native';
import { 
    BottomBar, Header
}                           from './../../homepage';
import ModalSelector        from 'react-native-modal-selector';
import FontAwesome          from 'react-native-vector-icons/FontAwesome';
import {
    initExams,
    getExamsbyPage
}                           from './../actions';
import { config }           from './../config';
import {
    changeScreen
}                           from './../../base/app';

class Exams extends React.Component{
    constructor(props){
        super(props);
    }
    static navigationOptions = {
        headerTitle: <Header />
    };
    
    UNSAFE_componentWillMount(){
        this.props.dispatch(initExams());
    }

    changePage = (page) => {
        this.props.dispatch(getExamsbyPage(page.key));
    }

    onPress = (screen,data) => () => {
        this.props.dispatch(changeScreen("Exams"));
        this.props.navigation.navigate(screen, data);
    }
    render(){
        let exams;
        exams = this.props.exams.find(d => d.page == this.props.page);
        return(
            <SafeAreaView style={{flex: 1}}>
                <StatusBar hidden={true} />
                <View style={{margin: 5}}>
                    <View style={{height: 30, flexDirection: "row"}}>
                        <View style={{flex: 6}}>
                            <Text style={{textTransform:"uppercase"}}>Danh sách đề thi</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: "row", justifyContent:"flex-end"}}>
                            <Text style={{lineHeight: 30, marginRight: 5}}>Trang</Text>
                            <ModalSelector
                                    data={this.props.listPage}
                                    initValue=""
                                    onChange={this.changePage}
                                    style={{width: 50, height: 30, borderWidth: 1, borderRadius: 15}}
                                >
                                <TextInput
                                    style={{ padding:0, width: 50, height:28, fontSize: 12, textAlign:'center', color: '#686'}}
                                    editable={false}
                                    underlineColorAndroid="transparent"
                                    placeholder={""}
                                    value={this.props.page+"/"+this.props.totalPage} />
                            </ModalSelector>
                        </View>
                    </View>
                    <ScrollView style={{width: (Dimensions.get('window').width-20), height: (Dimensions.get('window').height-160)}}>
                        {
                            exams != undefined &&
                            exams.data.map((e,index)=>{
                                return(
                                    <TouchableHighlight onPress={this.onPress("Exam",e)} key={index}>
                                        <View style={{flexDirection:"row", paddingHorizontal: 10, width: "100%", marginTop: 5}}>
                                            <FontAwesome style={{fontSize: 10, marginRight: 10, marginTop: 5}} name='circle' />
                                            <Text style={{color: "#000"}}>{e.title}</Text>
                                        </View>
                                    </TouchableHighlight>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomBar {...this.props} />
            </SafeAreaView>
        )
    }
}

function _mapStateToProps(state) {
    let listPage = [];
    if (1){
        for (var i = 0 ;i <= state['edubig/exam'].totalPage/config.totalPage; i++) {
            if (i == state['edubig/exam'].page-1){
                listPage.push({key:i+1, label:i+1+'', section:true});
            }else {
                listPage.push({key:i+1, label:i+1+''});
            }
        }
    }

    return {
        exams: state['edubig/exam'].exams.homepage,
        page  : state['edubig/exam'].page,
        listPage,
        totalPage: Math.ceil(state['edubig/exam'].totalPage/config.totalPage)
    };
}

export default connect(_mapStateToProps)(Exams);