import { get }                  from "./../base/portal";
import { config }               from './config';

export function getExams(page=1){
    return get("https://api.edubig.vn/api/v2/ebooks?type=EXAM&limit="+config.totalPage+"&page="+page);
}

export function countExam(){
    return get("https://api.edubig.vn/api/v2/ebook/count?type=EXAM");
}