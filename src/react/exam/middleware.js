
import { MiddlewareRegistry }   from '../base/redux';
import {
    setAsyncStorage
}                               from '../base/portal';

let mergeData = (type, exams, data, page) => {
    return new Promise(function(resolve, reject){
        switch (type) {
            case "HOMEPAGE":
                if(data.exams.homepage.findIndex(e => e.page == page) != -1){
                    resolve({
                        ...data,
                        exams:{
                            ...data.exams,
                            homepage: data.exams.homepage.map(exam => {
                                return exam.page == page ? {page: exam.page, data: exams} : exam
                            })
                        }
                    });
                }else{
                    resolve({
                        ...data,
                        exams: {
                            ...data.exams,
                            homepage: [...data.exams.homepage, {page, data: exams}]
                        }
                    });
                }
        }
    })
}

MiddlewareRegistry.register(store => next => action => {
    switch (action.type) {
        case "SET_EXAM":
            mergeData(action.typeLevel2, action.exams, action.data, action.page)
            .then(data => {
                setAsyncStorage("EXAM",JSON.stringify(data));
            })
            return next(action);
        default:
            return next(action);
    }
});