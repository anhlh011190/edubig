import { ReducerRegistry } from '../base/redux';
let stateDefault = {
    exams: {
        homepage: []
    },
    page: 1,
    totalPage: 1
}

ReducerRegistry.register('edubig/exam', (state = stateDefault, action) => {
    switch (action.type) {
        case "CHANGE_TOTAL_PAGE_EXAM":
            return {
                ...state,
                totalPage: action.totalPage
            }
        case "INIT_EXAM":
            return {
                ...state,
                ...action.exams
            }
        case "SET_EXAM":
            switch(action.typeLevel2){
                case "HOMEPAGE":
                    if(state.exams.homepage.findIndex(e => e.page == action.page) != -1){
                        return{
                            ...state,
                            page: action.page,
                            exams: {
                                ...state.exams,
                                homepage: state.exams.homepage.map(exam => {
                                    return exam.page == action.page ? {page: exam.page, data: action.exams} : exam
                                })
                            }
                        }
                    }else{
                        return{
                            ...state,
                            page: action.page,
                            exams: {
                                ...state.exams,
                                homepage: [...state.exams.homepage, {page: action.page, data: action.exams}]
                            }
                        }
                    }
                default:
                    return state;
            }
        default:
            return state;
    }
});