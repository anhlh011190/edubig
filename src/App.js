import React                from 'react';
import { Provider }         from 'react-redux';
import { createStore }      from 'redux';
import Thunk                from 'redux-thunk';
import { 
    Homepage
}                           from './react/homepage';
import { 
    Ebooks, Ebook,
    initEbooksStorage,
    initLogsStorage
}                           from './react/ebook';
import { 
    Exams, Exam,
    initExamsStorage
}                           from './react/exam';
import {
    Dashboard,
    History,
    SignIn,
    login
}                           from './react/user';
import {
    Theory,
    TheoryDetail,
    Quest
}                           from './react/theory';
import {
    getDeviceInformation
}                           from './react/base/devices';
import * as initApp         from './react/base/app';
import { 
    StateListenerRegistry, 
    ReducerRegistry, 
    MiddlewareRegistry 
}                               from './react/base/redux';
import { createAppContainer }   from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import io                       from "socket.io-client/dist/socket.io.js";
import {
    saveDeviceInfo,
    joinSocket
}                               from './react/base/app';
import {
    getAsyncStorage
}                               from './react/base/portal';           

let _createStore = () => {
    let middleware  = MiddlewareRegistry.applyMiddleware(Thunk);
    let reducer     = ReducerRegistry.combineReducers();
    let store       = createStore( reducer, middleware );
    StateListenerRegistry.subscribe(store);
    return store;
}
let store = _createStore();
let socket = io("http://socket.edubig.vn", { jsonp: false });
let deviceInfo = getDeviceInformation();
socket.emit("USER_JOIN", {userId: deviceInfo.uniqueID, fullname: deviceInfo.uniqueID});

store.dispatch(joinSocket(socket));

store.dispatch(saveDeviceInfo());
getAsyncStorage("EBOOKS").then(ebooks => {
    if(ebooks){
        store.dispatch(initEbooksStorage(JSON.parse(ebooks)));
    }
})
getAsyncStorage("LOGS").then(logs => {
    if(logs){
        store.dispatch(initLogsStorage(JSON.parse(logs)));
    }
})
getAsyncStorage("EXAM").then(exams => {
    if(exams){
        store.dispatch(initExamsStorage(JSON.parse(exams)));
    }
})
getAsyncStorage("USER").then(user => {
    if(user){
        let userInfo = JSON.parse(user);
        store.dispatch(login(userInfo.username, userInfo.password));
    }
})

const RootStack = createStackNavigator(
    {
        Homepage    : Homepage,
        Ebooks      : Ebooks,
        Exams       : Exams,
        Exam        : Exam,
        Ebook       : Ebook,
        Theory      : Theory,
        SignIn      : SignIn,
        Quest       : Quest,
        Dashboard   : Dashboard,
        History     : History,
        TheoryDetail: TheoryDetail
    },
    {
        initialRouteName: 'Homepage',
    }
);
  
const AppContainer = createAppContainer(RootStack);

class App extends React.Component{
    componentWillUpdate(){
        socket.disconnect();
    }

    render(){
        return(
            <Provider store={store}>
                <AppContainer />
            </Provider>
        )
    }
}

export default App;